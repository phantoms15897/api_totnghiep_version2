import { resolve } from "dns";

const bcrypt = require("bcrypt");
export function toObject(arr) {
  var rv = {};
  for (var i = 0; i < arr.length; ++i)
    rv[i] = arr[i];
  return rv;
}
export async function hashPassword(password) {
  const hashedPassword = await new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, function(err, hashpwd) {
        if (err) reject(err)
        resolve(hashpwd)
      });
  })
  return hashedPassword
}