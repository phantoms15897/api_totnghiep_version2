import { Car } from '../models/carModel';
import { Rate } from '../models/rateModel';
import { Order } from '../models/orderModel';

import { Request, Response, ErrorRequestHandler } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class CarController{

    public async getCarList    (req:Request,res:Response,err:ErrorRequestHandler) {           
        let page = req.query.page;
        page = page - 1;
        let query = {};
        if(req.query.city){
            query['city'] = req.query.city;
        }
        if(req.query.slot){
            query['slot'] = req.query.slot;
        }
        
        let minprice = Number(req.query.minprice) || 0;
        
        let maxprice = Number(req.query.maxprice) || 9999999999999;

        Car
        .find(query)
        .populate({path : 'user', populate : {path : 'user_details'}})
        .populate('rate')
        .populate('city')
        .where('price').gte(minprice).lte(maxprice)
        .sort({_id : -1})
        .skip(page*10)
        .limit(10)
        .exec()
        .then(car_list => {
            if(car_list.length >= 0)
            {   
                Car.find(query).where('price').gte(minprice).lte(maxprice).exec().then(result=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: car_list,
                        metadata: {
                            page: page+1,
                            total: result.length
                        }  
                    });
                })
                
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async getCarListUser    (req:Request,res:Response,err:ErrorRequestHandler) {           
        let page = req.query.page;
        page = page - 1;
        let query = {active:true};
        if(req.query.city){
            query['city'] = req.query.city;
        }
        if(req.query.slot){
            query['slot'] = req.query.slot;
        }
        
        let minprice = Number(req.query.minprice) || 0;
        
        let maxprice = Number(req.query.maxprice) || 9999999999999;

        Car
        .find(query)
        .populate({path : 'user', populate : {path : 'user_details'}})
        .populate('rate')
        .populate('city')
        .where('price').gte(minprice).lte(maxprice)
        .sort({_id : -1})
        .skip(page*10)
        .limit(10)
        .exec()
        .then(car_list => {
            if(car_list.length >= 0)
            {   
                Car.find(query).where('price').gte(minprice).lte(maxprice).exec().then(result=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: car_list,
                        metadata: {
                            page: page+1,
                            total: result.length
                        }  
                    });
                })
                
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async getCarListRate    (req:Request,res:Response,err:ErrorRequestHandler) {           
        let page = 1 || req.query.page;
        page = page - 1;
        let limit = 5 || req.query.limit;
        Car
        .find({active: true})
        .populate({path : 'user', populate : {path : 'user_details'}})
        .populate('rate')
        .populate('city')
        .sort({sumrate : -1})
        .skip(page*10)
        .limit(limit)
        .exec()
        .then(car_list => {
            if(car_list.length >= 0)
            {   
                Car.find().exec().then(result=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: car_list,
                        metadata: {
                            page: page+1,
                            total: result.length
                        }  
                    });
                })
                
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async getCarListPartner    (req:Request,res:Response,err:ErrorRequestHandler) {           
        let page = 1 || req.query.page;
        page = page - 1;
        let limit = 10 || req.query.limit;
        let partnerid = req.params.partnerid;
        Car
        .find({user: partnerid})
        .populate({path : 'user', populate : {path : 'user_details'}})
        .populate('rate')
        .populate('city')
        .sort({created_at : -1})
        .skip(page*10)
        .limit(limit)
        .exec()
        .then(car_list => {
            if(car_list.length >= 0)
            {   
                Car.find({user: partnerid}).exec().then(result=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: car_list,
                        metadata: {
                            page: page+1,
                            total: result.length
                        }  
                    });
                })
                
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async createCar     (req:Request,res:Response,err:ErrorRequestHandler) {
        const one_rate = new Rate()
        let id_rate = await one_rate.save().then(result =>{
            return result._id;
        })
        const one_car = new Car({
            brand: req.body.brand,
            user: req.body.user,
            car_name: req.body.car_name,
            license_plate: req.body.license_plate,
            color_car: req.body.color_car,
            slot: req.body.slot,
            city: req.body.city,
            speed: req.body.speed,
            fuel: req.body.fuel,
            type: req.body.type,
            rate: id_rate,
            dynamic: req.body.dynamic,
            image: req.body.image,
            capacity: req.body.capacity,
            function: req.body.function,
            procedure: req.body.procedure,
            pay: req.body.pay,
            sumrate: req.body.sumrate,
            price: req.body.price,
            note: req.body.note
        });
        one_car
        .save()
        .then(result => {
            Car
            .find({_id: result._id})
            .populate({path : 'user', populate : {path : 'user_details'}})
            .populate('rate')
            .populate('city')
            .exec()
            .then(result2=>{
                res.status(200).json({
                    status: "SUCCESS",
                    data: result2[0]
                });
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async getCar     (req:Request,res:Response,err:ErrorRequestHandler) {                
        const id = req.params.carid;
        Car
        .find({_id: id})
        .populate({path : 'user', populate : {path : 'user_details'}})
        .populate('rate')
        .populate('city')
        .exec()
        .then(one_car => {
            if(one_car.length > 0)
            {   
                Car.find().exec().then(result=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: one_car[0]
                    });
                })
                
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async findCar     (req:Request,res:Response,next:NextFunction) {        
        let page = req.query.page;
        page = page - 1;
        let query = {};
        if(req.body.city){
            query['slot'] = req.body.slot;
        }
        if(req.body.rate){
            query['rate'] = req.body.rate;
        }
        let price = 0;
        if(req.body.price){
            price = req.body.price;
        }
        Car
        .find(query)
        .populate({path : 'user', populate : {path : 'user_details'}})
        .populate('city')
        .where({ 'price': { $gt: price }})
        .sort({_id : -1})
        .skip(page ? page*10 : 0)
        .limit(10)
        .exec()
        .then(car_list => {
            if(car_list.length >= 0)
            {   
                Car.find().exec().then(result=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: car_list,
                        metadata: {
                            page: page+1,
                            total: result.length
                        }  
                    });
                })
                
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });                
    }
    public async updateCar     (req:Request,res:Response,err:ErrorRequestHandler) {                
        const id = req.params.carid;
        Car.updateOne(
            { _id: id }, 
            { $set: req.body },
            { new: true }
            )
            .exec()
            .then(result => {
                Car
                .find({_id: id })
                .populate('rate')
                .populate('city')
                .exec()
                .then(one_car =>{
                    if(one_car.length > 0){
                        res.status(200).json({
                            status: "SUCCESS",
                            data: one_car[0]
                        });
                    }
                })            
            })
            .catch(err => {
                res.status(500).json({
                    status: "FAIL",
                });
            });
    }
    public async deleteCar     (req:Request,res:Response,err:ErrorRequestHandler) {                
        const id = req.params.carid;
        Car.findOneAndDelete({ _id: id })
        .exec()
        .then(result => {
            Order.remove({ car:  result._id}).exec()
            res.status(200).json({
                status: "SUCCESS",
                data: result
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async maxprice     (req:Request,res:Response,err:ErrorRequestHandler) {                
        Car.find().sort({ price: -1 }).limit(1)
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result[0]
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async minprice     (req:Request,res:Response,err:ErrorRequestHandler) {                
        Car.find().sort({ price: 1 }).limit(1)
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result[0]
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
}