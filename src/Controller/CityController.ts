import { City } from '../models/cityModel';
import { Request, Response, ErrorRequestHandler } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class CityController{

    public async getCityList    (req:Request,res:Response,err:ErrorRequestHandler) {           
        City
        .find()
        .exec()
        .then(city_list => {
            if(city_list.length >= 0)
            {   
                res.status(200).json({
                    status: "SUCCESS",
                    data: city_list,
                });  
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async searchCityList    (req:Request,res:Response,err:ErrorRequestHandler) { 
        let city = req.query.city;
        city = `/^${city}/`;
        city = city.slice(1,city.length-1);
        console.log(city);    
        City
        .find({ "city_name": { "$regex": city, "$options": "i" } })
        .exec()
        .then(city_list => {
            if(city_list.length >= 0)
            {   
                res.status(200).json({
                    status: "SUCCESS",
                    data: city_list,
                });  
            }else{
                res.status(200).json({
                    status: "FAIL",
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async createCity     (req:Request,res:Response,err:ErrorRequestHandler) {
        const one_city = new City({
            city_name: req.body.city_name,
            city_nosen: req.body.city_nosen,
        });
        one_city
        .save()
        .then(result => {
            res.status(201).json({
                status: "SUCCESS"
            })
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
}