import { Comment } from '../models/commentModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class CommentController{
    
    public async getCommentList    (req:Request,res:Response,next:NextFunction) {           
        Comment
        .find()
        .exec()
        .then(comment_list => {
            if(comment_list.length > 0)
            {
                res.status(200).json(
                {
                    status: "SUCCESS",
                    data: comment_list
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async createComment     (req:Request,res:Response,next:NextFunction) {
        const one_comment = new Comment({
            user_id: req.body.user_id,
            news_id: req.body.news_id,
            body: req.body.body
        });
        one_comment
        .save()
        .then(result => {
            res.status(201).json({
                status: "SUCCESS"
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            })
        })
    }
    public async updateComment     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.commentid;
        Comment.updateOne(
            { _id: id }, 
            { $set: req.body },
            { new: true }
            )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
    }
    public async deleteComment     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.commentid;
        Comment.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                error: err
            });
        });
    }
}