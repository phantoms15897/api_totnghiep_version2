import { LocationTrip } from '../models/locationTripModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class LocationTripController{
        
    public async getLocationTripList    (req:Request,res:Response,next:NextFunction) {           
        LocationTrip
        .find({})
        .exec()
        .then(location_trip_list => {
            if(location_trip_list.length > 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: location_trip_list
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async getLocationTrip    (req:Request,res:Response,next:NextFunction) {           
        
    }
    public async createLocationTrip     (req:Request,res:Response,next:NextFunction) {
        var location_name = req.body.location_name;
        if(location_name != null && location_name.length > 0){
            LocationTrip
            .find({location_name: location_name})
            .exec()
            .then(result => {
                if(result.length == 0){
                    const one_location_trip = new LocationTrip({
                        trip_id: req.body.trip_id,
                        location_name: req.body.location_name,
                        lat: req.body.lat,
                        long: req.body.long,
                    });
                    one_location_trip
                    .save()
                    .then(result => {
                        res.status(200).json({
                            status: "SUCCESS"
                        })
                    })
                    .catch(err => {
                        res.status(500).json({
                            status: "FAIL"
                        });
                    });
                }else{
                    res.status(500).json({
                        status: "FAIL"
                    });
                }
            })
            .catch(err => {
                res.status(500).json({
                    status: "FAIL"
                });
            });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
    }
    public async searchLocationTrip   (req:Request,res:Response,next:NextFunction) {                
        
    }
    public async updateLocationTrip     (req:Request,res:Response,next:NextFunction) {                
        
    }
    public async deleteLocationTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.locationtripid;
        LocationTrip
        .remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
}