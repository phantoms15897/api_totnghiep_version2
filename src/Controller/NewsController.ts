import { News } from '../models/newsModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class NewsController{

    public async getNewsList    (req:Request,res:Response,next:NextFunction) {          
        let page = req.query.page;
        page = page - 1;
        News
        .find({})
        .populate({path : 'user', populate : {path : 'user_detail'}})
        .populate('type_news')
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {

            if(err){
                res.status(200).json({
                    status: "FAIL"
                })
            }
            News.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async createNews     (req:Request,res:Response,next:NextFunction) {
        News
        .find({ 
            title: req.body.title
        })
        .exec()
        .then(titles => {
            if(titles.length >= 1)
            {
                return res.status(409).json({
                    status: "FAIL",
                    message: "TITLE News exists"
                });
            }
            else
            {
                const one_news = new News({
                    user: req.body.user,
                    type_news: req.body.type_news,
                    title: req.body.title,
                    body_news: req.body.body_news,
                    images: req.body.images
                });
                one_news
                .save()
                .then(result => {
                    res.status(201).json({
                        status: "SUCCESS",
                        data:result
                    })
                })
                .catch(err => {
                    res.status(500).json({
                        status: "FAIL",
                    });
                });
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async searchNews   (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.newsid;
        News
        .find({_id: id})
        .exec()
        .then(result => {
            if(result.length >= 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: result
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                status: "FAIL"
            })
        });
    }
    public async updateNews     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.newsid;
        News.updateOne(
            { _id: id }, 
            {$set: req.body },
            { new: true }
            )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
    public async deleteNews     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.newsid;
        News.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
}