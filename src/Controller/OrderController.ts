import { Order } from '../models/orderModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
import { Car } from '../models/carModel';
const common = require("../Common/common");
export class OrderController{

    public async getOrderList    (req:Request,res:Response,next:NextFunction) {          
        let page = req.query.page;
        page = page - 1;
        Order
        .find({})
        .populate({path : 'user_order', populate : {path : 'user_details'}})
        .populate({path : 'car', populate : {path : 'rate'}})
        .populate('promotion')
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {
            if(err){
                res.status(200).json({
                    status: "FAIL"
                })
            }
            Order.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async getAllOrderPartner    (req:Request,res:Response,next:NextFunction) {          
        let id = req.params.partnerid;
        let page = 1 || req.query.page;
        page = page - 1;
        Order
        .find({})
        .populate({path : 'user_order', populate : {path : 'user_details'}})
        .populate({path : 'car',  match: {user:id},populate : {path : 'rate'}})
        .populate('promotion')
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {
            if(err){
                res.status(200).json({
                    status: "FAIL"
                })
            }
            Order.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                        page: page+1,
                        total: results.length
                    }
                })
            });
        });
    }
    public async getAllOrderUser    (req:Request,res:Response,next:NextFunction) {     
        console.log("user id");     
        let id = req.params.userid;
        let page = 1 || req.query.page;
        page = page - 1;
        Order
        .find({user_order:id})
        .populate({path : 'user_order', populate : {path : 'user_details'}})
        .populate({path : 'car', populate : {path : 'rate'}})
        .populate('promotion')
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {
            if(err){
                res.status(200).json({
                    status: "FAIL"
                })
            }
            Order.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                        page: page+1,
                        total: results.length
                    }
                })
            });
        });
    }
    public async createOrder     (req:Request,res:Response,next:NextFunction) {
        const one_order = new Order({
            car: req.body.car,
            user_order: req.body.user_order,
            cost: req.body.cost,
            promotion_cost: req.body.promotion_cost,
            day_number: req.body.day_number,
            received_date: req.body.received_date,
            pay_date: req.body.pay_date
        });
        one_order
        .save()
        .then(result => {
            Order.find({_id: result._id})
            .populate({path : 'user_order', populate : {path : 'user_details'}})
            .populate('car')
            .populate('promotion_code')
            .exec().then(result2=>{
                Car.updateOne(
                { _id: req.body.car }, 
                { $set: {active: false} }
                )
                .exec()
                res.status(201).json({
                    status: "SUCCESS",
                    data: result2[0]
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });
    }
    public async searchOrder   (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.orderid;
        Order
        .find({_id: id})
        .exec()
        .then(result => {
            if(result.length >= 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: result[0]
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                status: "FAIL"
            })
        });
    }
    public async updateOrder     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.orderid;
        console.log(id);
        Order.updateOne(
            { _id: id }, 
            { $set: req.body },
            { new: true }
        )
        .exec()
        .then(result => {
            Order.find({ _id: id })
            .populate({path : 'user', populate : {path : 'user_details'}})
            .populate('car')
            .populate('promotion_code')
            .exec()
            .then(result2 =>{
                res.status(201).json({
                    status: "SUCCESS",
                    data: result2[0]
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
    public async deleteOrder     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.orderid;
        Order.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
}