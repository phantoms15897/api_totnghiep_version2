import { Promotion } from '../models/promotionModel';
import { Car } from '../models/carModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class PromotionController{
    
    public async getPromotionList    (req:Request,res:Response,next:NextFunction) {     
        var datetime = new Date();       
        let page = req.query.page;
        page = page - 1;
        Promotion
        .find({})
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {

            if(err){
                res.json(err);
            }
            Promotion.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async getPromotionList2    (req:Request,res:Response,next:NextFunction) {     
        var datetime = new Date();       
        let page = req.query.page;
        page = page - 1;
        Promotion
        .find({})
        .sort({ created_at: -1})
        .where({ 'date_end': { $gt: datetime }})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {

            if(err){
                res.json(err);
            }
            Promotion.find({}).where({ 'date_end': { $gt: datetime }})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async getPromotionPartner    (req:Request,res:Response,next:NextFunction) {       
        let page = req.query.page;
        page = page - 1;
        let partnerid = req.params.partnerid;
        Promotion
        .find({user: partnerid})
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {

            if(err){
                res.json(err);
            }
            Promotion.find({user: partnerid})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async createPromotion     (req:Request,res:Response,next:NextFunction) {
        const one_promotion = new Promotion({
            user: req.body.user,
            title: req.body.title,
            description: req.body.description,
            image: req.body.image,
            promotion_cost: req.body.promotion_cost,
            code: Math.floor(Math.random() * Math.floor(999999)),
            amount: req.body.amount,
            unit: req.body.unit,
            date_end: req.body.date_end,
            date_start: req.body.date_start,
        });
        one_promotion
        .save()
        .then(result => {
            console.log(result);
            res.status(201).json({
                status: "SUCCESS",
                data: result
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async checkPromotion   (req:Request,res:Response,next:NextFunction) {                
        let promotioncode = req.body.promotioncode;
        let carid = req.body.carid;
        console.log(promotioncode);
        console.log(carid);
        let userpromotion = await Promotion
        .find({ code: promotioncode})
        .exec()
        .then(result => {
            console.log(result[0]['user']);
            return result[0]['user'];
        })
        .catch(err =>{
            console.log(err);
            return res.status(500).json({
                status: "FAIL"
            })
        });
        let usercar = await Car
        .find({ _id: carid})
        .exec()
        .then(result => {
            console.log(result[0]['user']);
            return result[0]['user'];
        })
        .catch(err =>{
            console.log(err);
            return res.status(500).json({
                status: "FAIL"
            })
        });  
        if(String(usercar) === String(userpromotion)){
            Promotion
            .find({ code: promotioncode})
            .exec()
            .then(result => {
                return res.status(200).json({
                    status: "SUCCESS",
                    data: result[0]
                })
            })
            .catch(err =>{
                console.log(err);
                return res.status(500).json({
                    status: "FAIL"
                })
            });
        } 
        else{
            return res.status(500).json({
                status: "FAIL"
            })
        } 
    }
    public async getPromotionCode   (req:Request,res:Response,next:NextFunction) {
        var datetime = new Date();                
        let code = req.params.promotioncode;
        console.log(code);
        Promotion
        .find({code: code})
        .where({ 'date_end': { $gt: datetime }})
        .exec()
        .then(result => {
            if(result.length > 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: result[0]
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                });
            }
        })
        .catch(err =>{
            console.log(err);
            res.status(500).json({
                status: "FAIL"
            })
        });      
    }
    public async searchPromotion   (req:Request,res:Response,next:NextFunction) {                
        let id = req.params.promotionid;
        Promotion
        .find({_id: id})
        .exec()
        .then(result => {
            if(result.length >= 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: result[0]
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                });
            }
        })
        .catch(err =>{
            console.log(err);
            res.status(500).json({
                status: "FAIL"
            })
        });      
    }
    public async updatePromotion     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.promotionid;
        Promotion.findOneAndUpdate(
            { _id: id }, 
            { $set: req.body },
            { new: true }
            )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async deletePromotion     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.promotionid;
        Promotion.findOneAndDelete({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
}