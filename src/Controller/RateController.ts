import { Rate } from '../models/rateModel';
import { Order } from '../models/orderModel';
import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class RateController{
    public async updateRate     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.rateid;
        let rating = req.body.rate;
        let tmp = null;
        if(rating == "rate_1"){
            tmp = { $inc: {rate_1:1}};
        }
        if(rating == "rate_2"){
            tmp = { $inc: {rate_2:1}};
        }
        if(rating == "rate_3"){
            tmp = { $inc: {rate_3:1}};
        }
        if(rating == "rate_4"){
            tmp = { $inc: {rate_4:1}};
        }
        if(rating == "rate_5"){
            tmp = { $inc: {rate_5:1}};
        }
        Rate.findOneAndUpdate(
            { _id: id }, 
            tmp ,
            { new: true }
        )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
}