import { Setting } from '../models/settingsModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class SettingController{
    
    public async getSetting    (req:Request,res:Response,next:NextFunction) {           
        Setting
        .find()
        .exec((err, result) => {

            if(err){
                res.json(err);
            }
            res.status(200).json({
                status: "SUCCESS",
                data: result[0]
            })
        });
    }
    public async createSetting     (req:Request,res:Response,next:NextFunction) {
        const one_setting = new Setting({
            question: req.body.question,
            banner: req.body.banner
        });
        one_setting
        .save()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async updateSetting     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.settingid;
        Setting.findOneAndUpdate(
            { _id: id }, 
            { $set: req.body.data },
            { new: true }
        )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
    public async deleteSetting     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.settingid;
        Setting.findOneAndDelete(
            { _id: id }
        )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
            });
        });
    }
}