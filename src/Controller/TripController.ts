import { Trip } from '../models/tripModel';

import { Request, Response,NextFunction } from 'express';
import {  } from 'connect';
const common = require("../Common/common");
const mongoose = require('mongoose');
export class TripController{
    
    public async getTripList    (req:Request,res:Response,next:NextFunction) { 
        var datetime = new Date();          
        let page = req.query.page;
        page = page - 1;
        let query = {};
        if(req.query.slot){
            query['slot'] = req.query.slot;
        }
        if(req.query.locationstart){
            query['location_start'] = req.query.location_start;
        }
        if(req.query.locationend){
            query['location_end'] = req.query.location_end;
        }
        Trip
        .find(query)
        .populate({path : 'owner', populate : {path : 'user_details'}})
        .populate({path : 'tripuser',populate : {path : 'user_details'}})
        .populate('location_start')
        .populate('location_end')
        .where({ 'date_start': { $gt: datetime }})
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {
            if(err){
                res.json(err);
            }
            Trip.find({})
                .exec()
                .then(results=>{
                    res.json({
                        status: "SUCCESS",
                        data: result,
                        metadata: {
                        page: page+1,
                        total: results.length
                        }
                    })
            });
        });   
    }
    public async getOneTrip    (req:Request,res:Response,next:NextFunction) { 
        const id = req.params.tripid;
        Trip
        .find({_id : id})
        .populate({path : 'owner', populate : {path : 'user_details'}})
        .populate({path : 'tripuser',populate : {path : 'user_details'}})
        .populate('location_start')
        .populate('location_end')
        .exec((err, result) => {
            if(err){
                res.json(err);
            }
            res.json({
                status: "SUCCESS",
                data: result[0]
            });
        });   
    }
    public async createTrip     (req:Request,res:Response,next:NextFunction) {
        const one_trip = new Trip({
            car: req.body.car,
            type_rent: req.body.type_rent,
            date_start: req.body.date_start,
            date_end: req.body.date_end,
            location_start: req.body.location_start,
            location_end: req.body.location_end,
            description: req.body.description,
            slot: req.body.slot,
            cost: req.body.cost,
            owner: req.body.owner
        });
        one_trip
        .save()
        .then(result => {
            Trip.find({_id: result._id})
            .populate({path : 'owner', populate : {path : 'user_details'}})
            .populate('location_start')
            .populate('location_end')
            .exec().then(result2=>{
                res.status(201).json({
                    status: "SUCCESS",
                    data: result2[0]
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        }); 
    }
    public async searchTrip     (req:Request,res:Response,next:NextFunction) {   
        // console.log(req.body.page)
        let datetime = new Date(); 
        let page = req.query.page;
        page = page - 1;         
        let query = {};
        if(req.query.slot !="undefined"){
            query['slot'] = req.query.slot;
        }
        if(req.query.location_start!= "undefined"){
            query['location_start'] = req.query.location_start;
        }
        if(req.query.location_end != "undefined"){
            query['location_end'] = req.query.location_end;
        }
        if(req.query.slot === "0"){
            delete query['slot'];
        }
        Trip
        .find(query)
        .populate({path : 'owner', populate : {path : 'user_details'}})
        .populate({path : 'tripuser',populate : {path : 'user_details'}})
        .populate('location_start')
        .populate('location_end')
        .where({ 'date_start': { $gt: datetime }})
        .sort({ created_at: -1})
        .skip( page ? 10*page : 0)
        .limit(10)
        .exec((err, result) => {
            if(err){
                res.json(err);
            }
            Trip.find({})
                .exec()
                .then(results=>{
                    res.json({
                        status: "SUCCESS",
                        data: result,
                        metadata: {
                        page: page+1,
                        total: results.length
                        }
                    })
            });
        });               
    }
    public async updateTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.tripid;
        Trip.updateOne(
            { _id: id }, 
            { $set: req.body },
            { new: true }
        )
        .exec()
        .then(result => {
            Trip.find({_id: id})
            .populate({path : 'owner', populate : {path : 'user_details'}})
            .populate({path : 'tripuser',populate : {path : 'user_details'}})
            .populate('location_start')
            .populate('location_end')
            .exec()
            .then(result2=>{
                res.status(200).json({
                    status: "SUCCESS",
                    data: result2[0]
                })
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
    public async joinTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.tripid;
        Trip.find({ _id: id })
        .exec()
        .then(result => {
            let listuser = result[0]['tripuser'];
            let user = req.body.tripuser;

            if(listuser.indexOf(user) < 0 && user != null && user != result[0]['owner']){
                listuser.push(req.body.tripuser);
            }
            else{
                return res.status(200).json({
                    status: "FAIL",
                    message: "Invalidate"
                });
            }
            Trip.findOneAndUpdate({ _id: id }, { tripuser: listuser },{ new: true })
            .exec()
            .then(result => {
                Trip.find({_id: id})
                .populate({path : 'owner', populate : {path : 'user_details'}})
                .populate({path : 'tripuser',populate : {path : 'user_details'}})
                .populate('location_start')
                .populate('location_end')
                .exec().then(result2=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: result2[0]
                    })
                })
            })
            .catch(err => {
                res.status(200).json({
                    status: "FAIL"
                });
            });
        })
        
    }
    public async unjoinTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.tripid;
        Trip.find({ _id: id })
        .exec()
        .then(result => {
            let listuser = result[0]['tripuser'];
            let user = req.body.tripuser;
            console.log(user);
            if(listuser.indexOf(user) !== -1 && user != "" && user != result[0]['owner']){
                listuser.splice( listuser.indexOf(user), 1 );
            }
            else{
                return res.status(200).json({
                    status: "FAIL",
                    message: "Invalidate"
                });
            }
            Trip.findOneAndUpdate({ _id: id }, { tripuser: listuser },{ new: true })
            .exec()
            .then(result => {
                Trip.find({_id: id})
                .populate({path : 'owner', populate : {path : 'user_details'}})
                .populate('location_start')
                .populate('location_end') 
                .populate({path : 'tripuser',populate : {path : 'user_details'}})
                .exec().then(result2=>{
                    res.status(200).json({
                        status: "SUCCESS",
                        data: result2[0]
                    })
                })
            })
            .catch(err => {
                res.status(200).json({
                    status: "FAIL"
                });
            });
        })
    }
    //Search trip join
    public async searchTripJoin     (req:Request,res:Response,next:NextFunction) {              
        let tripuser = req.params.userid;
        console.log(tripuser);
        Trip
        .find({tripuser: {$in:tripuser}})
        .populate({path : 'owner', populate : {path : 'user_details'}})
        .populate({path : 'tripuser',populate : {path : 'user_details'}})
        .populate('location_start')
        .populate('location_end')
        .exec()
        .then(result => {
            if(result.length >= 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: result
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
        })
        .catch(err =>{
            res.status(500).json({
                status: "FAIL"
            })
        });
    }
    public async searchTripCreate     (req:Request,res:Response,next:NextFunction) {                
        let owner = req.params.userid;
        Trip
        .find({owner: owner})
        .populate({path : 'owner', populate : {path : 'user_details'}})
        .populate({path : 'tripuser',populate : {path : 'user_details'}})
        .populate('location_start')
        .populate('location_end')
        .exec()
        .then(result => {
            if(result.length >= 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: result
                });
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
        })
        .catch(err =>{
            res.status(500).json({
                status: "FAIL"
            })
        });
    }
    public async deleteTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.tripid;
        Trip.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS",
                data: result
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
}