import { TypeNews } from '../models/typeNewsModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class TypeNewsController{
    
    public async getTypeNewsList    (req:Request,res:Response,next:NextFunction) {           
        let page = req.query.page;
        page = page - 1;
        TypeNews
        .find({})
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {

            if(err){
                res.status(200).json({
                    status: "FAIL"
                })
            }
            TypeNews.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async createTypeNewsTrip     (req:Request,res:Response,next:NextFunction) {
        TypeNews
        .find({ 
            type_name: req.body.type_name
        })
        .exec()
        .then(type_names => {
            if(type_names.length >= 1)
            {
                return res.status(409).json({
                    status: "FAIL",
                    message: "Type name exists"
                });
            }
            else
            {
                const one_type_news = new TypeNews({
                    type_name: req.body.type_name,
                });
                one_type_news
                .save()
                .then(result => {
                    res.status(201).json({
                        status: "SUCCESS",
                        data:result
                    })
                })
                .catch(err => {
                    res.status(500).json({
                        status: "FAIL"
                    });
                });
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                status: "FAIL"
            })
        });
    }
    public async searchTypeNews   (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.typenewsid;
        TypeNews
        .find({_id: req.params.typenewsid})
        .exec()
        .then(one_type => {
            if(one_type.length >= 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: one_type
                });
            }else{
                res.status(200).json({
                    message: "No entries found"
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                error: err
            })
        });      
    }
    public async updateTypeNews     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.typenewsid;
        TypeNews.updateOne(
            { _id: id }, 
            { $set: req.body },
            { new: true }
            )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
    public async deleteTypeNews     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.typenewsid;
        TypeNews.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
}