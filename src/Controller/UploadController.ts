
import { Request, Response,ErrorRequestHandler } from 'express';
import { NextFunction, ErrorHandleFunction } from 'connect';
const common = require("../Common/common");
const fs = require("fs");
export class UploadController{
    
    
    public async uploadSingle    (req:Request,res:Response,err:ErrorRequestHandler) {  
        // console.log(req);         
        const file = req['file']
        if (!file) {
            res.status(500).json({
                status: "FAIL"
            })
        }
        res.status(200).json({
            status: "SUCCESS",
            data: file.path
        })
    }
    
    public async uploadMulti     (req:Request,res:Response,err:ErrorRequestHandler) {
        const files = req['files']
        let image_path = [];
        for(let i = 0; i < files.length; i++){
            // image_path.push(files[i].toString('base64'));
            image_path.push(files[i]);
        }
        if (!files) {
            res.status(500).json({
                status: "FAIL"
            })
        }
        res.status(200).json({
            status: "SUCCESS",
            data: image_path
        })
    }
}
