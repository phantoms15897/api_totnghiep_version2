import { User } from '../models/userModel';
import { UserDetail } from '../models/userDetailModel';
import { Promotion } from '../models/promotionModel';
import { Car } from '../models/carModel';
import { Order } from '../models/orderModel';
import { Trip } from '../models/tripModel';


import { Request, Response } from 'express';
import { NextFunction } from 'connect';
import { ErrorRequestHandler } from 'express-serve-static-core';
const common = require("../Common/common");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
export class UserController{
    public async getAllUser (req:Request,res:Response,next:NextFunction) {           
        let page = req.query.page;
        page = page - 1;
        let query = {};
        if(req.query.role){
            query['role'] = req.query.role;
        }
        User
        .find(query)
        .populate('user_details')
        .sort({ created_at: -1})
        .skip(10*page)
        .limit(10)
        .exec((err, result) => {

            if(err){
                res.json(err);
            }
            User.find({})
                .exec()
                .then(results=>{
                res.json({
                    status: "SUCCESS",
                    data: result,
                    metadata: {
                    page: page+1,
                    total: results.length
                    }
                })
            });
        });
    }
    public async signUp     (req:Request,res:Response,next:NextFunction) {
        User
        .find({
            username: req.body.username
        })
        .exec()
        .then(user => {
            if(user.length >= 1) {
                return res.status(200).json({
                    status: "FAIL",
                    message: "Account exists",
                    exist: true
                });
            }
            else
            {
                const user_detail = new UserDetail({
                    name: req.body.name,
                    birthday: req.body.birthday,
                    address: req.body.address,
                    gender: req.body.gender,
                    phone: req.body.phone,
                    email: req.body.email,
                    image: req.body.image
                })
                user_detail
                .save()
                .then(async result=>{
                    const user = new User({
                        username: req.body.username,
                        password: await common.hashPassword(req.body.password),
                        role: req.body.role,
                        user_details: result._id
                    });
                    user
                    .save()
                    .then(result => {
                        return res.json({
                            status: "SUCCESS",
                            data: result
                        });
                    })
                })
                
            }
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });
    }
    public async findUser   (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.userid;
        User
        .findOne({_id: id})
        .populate('user_details')
        .exec((err, result) => {
            if(err){
                res.status(500).json({
                    status: "FAIL",
                    error: err
                });
            }    
            res.json({
                status:"SUCCESS",
                data: result
            });
        });
    }
    public async updateUser (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.userid;
        var update_user = {};
        if(req.body.password && req.body.role){
            const pwd = await common.hashPassword(req.body.password);
            update_user = {
                password: pwd,
                role: req.body.role
            }
        }else if(req.body.role){
            update_user = {
                role: req.body.role
            }
        }else if(req.body.password){
            const pwd = await common.hashPassword(req.body.password);
            update_user = {
                password: pwd
            }
        }
        User
        .findOneAndUpdate(
            { _id: id },
            { $set:  update_user},
            { new: true }
        )
        .exec()
        .then(result => {
            UserDetail
            .findOneAndUpdate({ _id: result.user_details  }, { $set: req.body })
            .exec()
            .then(results => {
                User
                .find({ _id: req.params.userid })
                .populate("user_details")
                .exec()
                .then(profile => {
                    if (profile.length > 0) {
                        res.status(200).json({
                            status: "SUCCESS",
                            data: profile[0]
                        });
                    } else {
                        res.status(200).json({
                            status: "FAIL"
                        })
                    }

                })
                .catch(err => {
                    res.status(500).json({
                        status: "FAIL",
                        error: err
                    })
                });
            })
            .catch(err => {
                res.status(500).json({
                    status: "FAIL",
                    error: err
                });
            });
        })
    }
    public async logIn      (req:Request,res:Response,next:NextFunction) {                
        User
        .find({ username: req.body.username })
        .populate('user_details')
        .exec()
        .then(async user => {
            if (user.length < 1) {
                return res.status(401).json({
                    status: "FAIL",
                    message: "Not Found Entry",
                });
            }
            await bcrypt.compare(req.body.password, user[0].password, (err, result) => {

                if (err) {
                    return res.status(401).json({
                        status: "FAIL",
                        message: "Auth failed"
                    });
                }
                if (!result) {
                    return res.status(401).json({
                        status: "FAIL",
                        message: "Auth failed"
                    });
                }
                const token = jwt.sign(
                    {
                        username: user[0].email,
                        user: user[0]
                    },
                    "secret",
                    {
                        expiresIn: "1h"
                    }
                );
                User
                .find({ _id: user[0]._id })
                .populate('user_details')
                .exec()
                .then(result => {
                    return res.status(200).json({

                        status: "SUCCESS",
                        data: {
                            token: token,
                            user_data: result[0]
                        }
                    });
                })
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });
    }
    public async deleteUser (req:Request,res:Response,next:NextFunction) {              
        const id = req.params.userid;
        User.findOneAndDelete({ _id: id })
            .exec()
            .then(result => {
                UserDetail.remove({ _id:  result.user_details}).exec()
                Promotion.remove({ user:  result._id}).exec()
                Car.remove({ user:  result._id}).exec()
                Order.remove({ user_order:  result._id}).exec()
                Trip.remove({ owner:  result._id}).exec()
                
                res.status(200).json({
                    status: "SUCCESS"
                });
            })
            .catch(err => {
                res.status(500).json({
                    status: "FAIL",
                    error: err
                });
            });
    }
}