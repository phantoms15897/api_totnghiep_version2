import { UserTrip } from '../models/userTripModel';

import { Request, Response } from 'express';
import { NextFunction } from 'connect';
const common = require("../Common/common");
export class UserTripController{

    public async getUserTripList    (req:Request,res:Response,next:NextFunction) {           
        UserTrip
        .find()
        .exec()
        .then(user_trip_list => {
            if(user_trip_list.length > 0)
            {
                res.status(200).json({
                    status: "SUCCESS",
                    data: user_trip_list
                }
                );
            }else{
                res.status(200).json({
                    status: "FAIL"
                })
            }
            
        })
        .catch(err =>{
            res.status(500).json({
                status: "FAIL"
            })
        });
    }
    public async createUserTrip     (req:Request,res:Response,next:NextFunction) {
        const one_user_trip = new UserTrip({
            user_id: req.body.user_id,
            trip_id: req.body.trip_id,
        });
        one_user_trip
        .save()
        .then(result => {
            res.status(201).json({
                status: "SUCCESS"
            })
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
    public async searchUserOnTrip   (req:Request,res:Response,next:NextFunction) {                
        
    }
    public async updateUserTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.usertripid;
        UserTrip
        .updateOne(
            { _id: id },
            { $set: req.body },
            { new: true }
            )
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
    public async deleteUserTrip     (req:Request,res:Response,next:NextFunction) {                
        const id = req.params.usertripId;
        UserTrip.remove({ _id: id })
        .exec()
        .then(result => {
            res.status(200).json({
                status: "SUCCESS"
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL"
            });
        });
    }
}