import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import { Auth } from "./midlleware/Auth";
class App {
    public morgan = require('morgan');
    public cors = require('cors');
    public app: express.Application;
    public path = require('path');
    public mongoUrl: string = 'mongodb://localhost/api_totnghiep';
    // public mongoUrl: string = "mongodb+srv://trongpq:trong123@cluster0-3ojj5.mongodb.net/api_totnghiep?retryWrites=true";
    public rateRouter           = require('./routers/RateRouter');
    public orderRouter           = require('./routers/OrderRouter');
    public cityRouter           = require('./routers/CityRouter');
    public userRouter           = require('./routers/UserRouter');
    public carRouter            = require('./routers/CarRouter');
    public commentRouter        = require('./routers/CommentRouter');
    public locationTripRouter   = require('./routers/LocationTripRouter');
    public newsRouter           = require('./routers/NewsRouter');
    public promotionRouter      = require('./routers/PromotionRouter');
    public tripRouter           = require('./routers/TripRouter');
    public typeNewsRouter       = require('./routers/TypeNewsRouter');
    public userTripRouter       = require('./routers/UserTripRouter');
    public uploadRouter         = require('./routers/UploadRouter');
    public settingRouter         = require('./routers/SettingRouter');
    public auth = new Auth();
    constructor() {
        
        this.app = express();
        this.config(); 
        this.mongoSetup();      
        this.routerAPI();
    }

    private config(): void{
        this.app.use(this.morgan('dev'));
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(this.cors({
            'origin': '*',
            'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
            'preflightContinue': false
          }));
        this.app.use((req,res,next)=>{
            res.header('Access-Control-Allow-Origin','*');
            res.header(
                'Access-Control-Allow-Header',
                'Origin, X-Requested-With, Content-ype,Accept,Authorization'
            );
            if(req.method === 'OPTIONS'){
                res.header('Access-Control-Allow-Methods','PUT,POST,PATCH,DELETE,GET');
                return res.status(200).json({
        
                })
            }
            next();
        })
    }
    private mongoSetup(): void{
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl,{ useNewUrlParser: true }); 
        mongoose.set('useCreateIndex', true);
    }
    private routerAPI(): void{
        //Router User
        // this.app.use("/auth", this.auth.Check_User);
        // this.app.use("/api", this.auth.Check_Token);
        this.app.use("/rate", this.rateRouter);
        this.app.use("/settings", this.settingRouter);
        this.app.use("/order", this.orderRouter);
        this.app.use("/city", this.cityRouter);
        this.app.use("/user", this.userRouter);
        this.app.use("/car", this.carRouter);
        this.app.use("/comment", this.commentRouter);
        this.app.use("/trip", this.tripRouter);
        this.app.use("/typenews", this.typeNewsRouter);
        this.app.use("/promotion", this.promotionRouter);
        this.app.use("/news", this.newsRouter);
        this.app.use("/location", this.locationTripRouter);
        this.app.use("/comment", this.commentRouter);
        this.app.use('/uploads',express.static('./uploads'));

        this.app.use("/uploads", this.uploadRouter);
        //Router Admin
        this.app.use("/auth", this.auth.Check_User);
        this.app.use("/api", this.auth.Check_Token);
        this.app.use("/api/user", this.userRouter);
        this.app.use("/api/car", this.carRouter);
        this.app.use("/api/comment", this.commentRouter);
        this.app.use("/api/trip", this.tripRouter);
        this.app.use("/api/typenews", this.typeNewsRouter);
        this.app.use("/api/promotion", this.promotionRouter);
        this.app.use("/api/news", this.newsRouter);
        this.app.use("/api/location", this.locationTripRouter);
        this.app.use("/api/comment", this.commentRouter);
        this.app.use('/api/uploads',express.static(__dirname+'/uploads/images'))
        this.app.use("/api/uploads", this.uploadRouter);
        //Route 404
        this.app.use((req,res,next)=>{
            let error = new Error("Not Found");
            error['status'] = 404;
            next(error);
        });
        this.app.use((error,req,res, next)=>{
            res.status(error.status || 500);
            res.json({
                error: {
                    message: error.message
                }
            })
        });
        
    }
}

export default new App().app;