import { Request, Response } from 'express';
import { NextFunction } from 'connect';
import { User } from '../models/userModel';

const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');

export class Auth{

    public async Check_Token (req:Request,res:Response,next:NextFunction) {
        try {
            const token = req.headers.authorization;
            const decoded = jwt.verify(token, "secret");
            delete decoded['user'].password;
            res.locals.userdata = decoded['user'];
            next();
        } catch (error) {
            return res.status(401).json({
                status: "FAIL",
                message: 'Auth failed'
            });
        }
    };
    public async Check_User (req:Request,res:Response,next:NextFunction) {
        User
        .find({ username: req.body.username })
        .populate('user_detail')
        .exec()
        .then(async user => {
            if (user.length < 1) {
                return res.status(401).json({
                    status: "FAIL",
                    message: "Not Found Entry",
                });
            }
            await bcrypt.compare(req.body.password, user[0].password, (err, result) => {

                if (err) {
                    return res.status(401).json({
                        status: "FAIL",
                        message: "Auth failed"
                    });
                }
                if (!result) {
                    return res.status(401).json({
                        status: "FAIL",
                        message: "Auth failed"
                    });
                }
                const token = jwt.sign(
                    {
                        username: user[0].email,
                        user: user[0]
                    },
                    "secret",
                    {
                        expiresIn: "1h"
                    }
                );
                User
                .find({ _id: user[0]._id })
                .populate('user_detail')
                .exec()
                .then(user_detail => {
                    return res.status(200).json({

                        status: "SUCCESS",
                        data: {
                            token: token,
                            user_data: user_detail[0]
                        }
                    });
                })
            });
        })
        .catch(err => {
            res.status(500).json({
                status: "FAIL",
                error: err
            })
        });
    };
}