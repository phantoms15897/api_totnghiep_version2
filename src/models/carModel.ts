import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const CarSchema = new Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'User'
    },
    car_name: { 
        type: String, 
        require : ""
    },
    brand: { 
        type: String, 
        default : ""
    },
    license_plate: { 
        type: String, 
        default : ""
    },
    color_car: { 
        type: String, 
        default : ""
    },
    slot: { 
        type: Number, 
        default : 0
    },
    city: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'City'
    },
    speed: { 
        type: Number, 
        default : 0
    },
    fuel: { 
        type: String, 
        default : "0"
    },
    type: { 
        type: String, 
        default : ""
    },
    dynamic: { 
        type: Number, 
        default : 0
    },
    capacity: { 
        type: Number, 
        default : 0
    },
    function: { 
        type: Array, 
        default : []
    },
    procedure: { 
        type: Array, 
        default : []
    },
    pay: { 
        type: Array, 
        default : []
    },
    image: { 
        type: String, 
        default : ""
    },
    rate: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'Rate'
    },
    sumrate: { 
        type: Number, 
        default: 0
    },
    note: { 
        type: String, 
        default : ""
    },
    active: { 
        type: Boolean, 
        default : true
    },
    price: { 
        type: Number, 
        default : 0
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

export const Car = mongoose.model('Car', CarSchema);