import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const CitySchema = new Schema({
    city_name: { 
        type: String, 
        default : ''
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

export const City = mongoose.model('City', CitySchema);