import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    news: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'news'
    },
    body: { 
        type: String, 
        default : ""
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const Comment = mongoose.model('Comment', CommentSchema);