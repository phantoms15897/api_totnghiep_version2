import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const LocationTripSchema = new Schema({
    trip: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'trip',
        required: true,
    },
    location_name: { 
        type: String, 
        default : ''
    },
    lat: { 
        type: Number, 
        default : 0
    },
    long: { 
        type: Number, 
        default : 0
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const LocationTrip = mongoose.model('LocationTrip', LocationTripSchema);