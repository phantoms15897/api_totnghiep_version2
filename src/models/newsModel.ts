import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const NewsSchema = new Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'User'
    },
    type_news: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'TypeNews'
    },
    title: {
        type: String,  
        default: ""
    },
    body_news: {
        type: String,  
        default: ""
    },
    views: {
        type: Number,  
        default: 0
    },
    images: {
        type: String,  
        default: ""
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const News = mongoose.model('News', NewsSchema);