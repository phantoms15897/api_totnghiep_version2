import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    user_order: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'User'
    },
    car: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'Car'
    },
    promotion_cost: {
        type: Number,  
        default: 0
    },
    cost: {
        type: Number,  
        default: ""
    },
    day_number: {
        type: String,  
        default: ""
    },
    is_rate: {
        type: Boolean,  
        default: false
    },
    received_date: {
        type: Date,  
        default: Date.now
    },
    pay_date: {
        type: Date,  
        default: Date.now
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

export const Order = mongoose.model('Order', OrderSchema);