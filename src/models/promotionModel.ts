import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const PromotionSchema = new Schema({
    user: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'User'
    },
    title: { 
        type: String, 
        default : ''
    },
    description: { 
        type: String, 
        default : ''
    },
    image: { 
        type: String, 
        default : ''
    },
    code: { 
        type: String, 
        default : ''
    },
    amount: { 
        type: Number, 
        default : 0
    },
    unit: { 
        type: String, 
        default : '%'||'VND'
    },
    date_start: { 
        type: Date, 
        default : Date.now
    },
    date_end: { 
        type: Date, 
        default : Date.now
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const Promotion = mongoose.model('Promotion', PromotionSchema);