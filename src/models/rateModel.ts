import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const RateSchema = new Schema({
    rate_1: { 
        type: Number, 
        default : 0
    },
    rate_2: { 
        type: Number, 
        default : 0
    },
    rate_3: { 
        type: Number, 
        default : 0
    },
    rate_4: { 
        type: Number, 
        default : 0
    },
    rate_5: { 
        type: Number, 
        default : 0
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

export const Rate = mongoose.model('Rate', RateSchema);