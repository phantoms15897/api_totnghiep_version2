import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const SettingSchema = new Schema({
    banner: { 
        type: Array, 
        default : []
    },
    question:{
        type: Array,
        default: []
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const Setting = mongoose.model('Setting', SettingSchema);