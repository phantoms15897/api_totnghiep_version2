import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const TripSchema = new Schema({
    car: { 
        type: String, 
        default : ""
    },
    type_rent: { 
        type: String, 
        default : ""
    },
    date_start: { 
        type: Date, 
        default : Date.now
    },
    date_end: { 
        type: Date, 
        default : Date.now
    },
    location_start: { 
        type:  mongoose.Schema.Types.ObjectId, 
        ref: 'City',
        default : null
    },
    location_end: { 
        type:  mongoose.Schema.Types.ObjectId, 
        ref: 'City',
        default: null
    },
    cost: { 
        type: Number, 
        default : 0
    },
    tripuser:[{
        type:  mongoose.Schema.Types.ObjectId, 
        ref: 'User',
        default:[]
    }],
    slot: {
        type: Number,
        default: 0
    },
    owner:{
        type:  mongoose.Schema.Types.ObjectId, 
        ref: 'User'
    },
    description:{
        type: String, 
        ref: ""
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const Trip = mongoose.model('Trip', TripSchema);