import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const TypeNewsSchema = new Schema({
    type_name: { 
        type: String, 
        require : true
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const TypeNews = mongoose.model('TypeNews', TypeNewsSchema);