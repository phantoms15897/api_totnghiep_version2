import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserDetailSchema = new Schema({
    name: {
        type: String,
        default: null    
    },
    birthday: {
        type: Date,
        default: null    
    },
    address: {
        type: String,
        default: null    
    },
    gender: {
        type: String,
        default: null    
    },
    phone: {
        type: String,
        default: null    
    },
    email: { 
        type: String, 
        lowercase: true, 
        default:null
    },
    image: {
        type: String,
        default: null
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

export const UserDetail = mongoose.model('UserDetail', UserDetailSchema);