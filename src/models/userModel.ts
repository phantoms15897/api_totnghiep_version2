import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        lowercase: true,
        required: true,
        unique: true,
        update: false
    },
    password: {
        type: String,  
        required: true
    },
    role: {
        type: String,
        default: "user"
    },
    user_details:{
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'UserDetail'
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, default: 
        Date.now 
    }
});

export const User = mongoose.model('User', UserSchema);