import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserTripSchema = new Schema({
    user_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'user'
    },
    trip_id: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref : 'trip'
    },
    created_at: { 
        type: Date, 
        default: Date.now 
    },
    modifined_at: { 
        type: Date, 
        default: Date.now 
    }
});

export const UserTrip = mongoose.model('UserTrip', UserTripSchema);