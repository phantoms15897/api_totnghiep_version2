import * as express from "express";
import { CarController } from "../Controller/CarController";

const router = express.Router();

const carController: CarController = new CarController();

router.get('/',                     carController.getCarList);
router.get('/getlistuser',          carController.getCarListUser);
router.get('/getlistcarrate',       carController.getCarListRate);
router.get('/getlistcar/:partnerid',       carController.getCarListPartner);
router.get('/searchCar/',           carController.findCar);
router.get('/maxprice/',            carController.maxprice);
router.get('/minprice/',            carController.minprice);
router.get('/:carid',               carController.getCar);
router.patch('/:carid',             carController.updateCar);
router.post('/',                    carController.createCar);
router.delete('/:carid',            carController.deleteCar);


module.exports = router;