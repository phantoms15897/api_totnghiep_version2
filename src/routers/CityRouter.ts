import * as express from "express";
import { CityController } from "../Controller/CityController";

const router = express.Router();

const cityController: CityController = new CityController();

router.get('/',    cityController.getCityList);
router.get('/search',    cityController.searchCityList);
router.post('/',   cityController.createCity);



module.exports = router;