import * as express from "express";
import { CommentController } from "../Controller/CommentController";

const router = express.Router();

const commentController: CommentController = new CommentController();

router.get('/',             commentController.getCommentList);
router.patch('/:commentid',    commentController.updateComment);
router.post('/',      commentController.createComment);
router.delete('/:commentid',   commentController.deleteComment);


module.exports = router;