import * as express from "express";
import { LocationTripController } from "../Controller/LocationTripController";

const router = express.Router();

const locationTripController: LocationTripController = new LocationTripController();

router.get('/',                     locationTripController.getLocationTripList);
router.get('/:locationtripid',      locationTripController.getLocationTrip);
router.patch('/:locationtripid',    locationTripController.updateLocationTrip);
router.post('/',                    locationTripController.createLocationTrip);
router.delete('/:locationtripid',   locationTripController.deleteLocationTrip);


module.exports = router;