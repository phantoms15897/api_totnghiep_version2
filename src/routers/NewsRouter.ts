import * as express from "express";
import { NewsController } from "../Controller/NewsController";

const router = express.Router();

const newsController: NewsController = new NewsController();

router.get('/',             newsController.getNewsList);
router.get('/:newsid',      newsController.searchNews);
router.patch('/:newsid',    newsController.updateNews);
router.post('/',            newsController.createNews);
router.delete('/:newsid',   newsController.deleteNews);


module.exports = router;