import * as express from "express";
import { OrderController } from "../Controller/OrderController";

const router = express.Router();

const orderController: OrderController = new OrderController();

router.get('/',             orderController.getOrderList);
router.get('/:orderid',      orderController.searchOrder);
router.get('/getallorderpartner/:partnerid',      orderController.getAllOrderPartner);
router.get('/getallorderuser/:userid',      orderController.getAllOrderUser);
router.patch('/:orderid',    orderController.updateOrder);
router.post('/',            orderController.createOrder);
router.delete('/:orderid',   orderController.deleteOrder);


module.exports = router;