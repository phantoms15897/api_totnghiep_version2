import * as express from "express";
import { PromotionController } from "../Controller/PromotionController";

const router = express.Router();

const promotionController: PromotionController = new PromotionController();

router.get('/',             promotionController.getPromotionList);
router.get('/listpromotion',             promotionController.getPromotionList2);
router.get('/promotionpartner/:partnerid',      promotionController.getPromotionPartner);
router.get('/:promotionid',      promotionController.searchPromotion);
router.get('/promotioncode/:promotioncode',      promotionController.getPromotionCode);
router.patch('/:promotionid',    promotionController.updatePromotion);
router.post('/',            promotionController.createPromotion);
router.post('/checkpromotion',            promotionController.checkPromotion);
router.delete('/:promotionid',   promotionController.deletePromotion);


module.exports = router;