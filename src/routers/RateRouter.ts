import * as express from "express";
import { RateController } from "../Controller/RateController";

const router = express.Router();

const rateController: RateController = new RateController();

router.patch('/:rateid',    rateController.updateRate);



module.exports = router;