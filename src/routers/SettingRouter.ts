import { SettingController } from "../Controller/SettingController";
import * as express from "express";
const router = express.Router();

const settingController: SettingController = new SettingController();

router.get('/',             settingController.getSetting);
router.patch('/:settingid',    settingController.updateSetting);
router.post('/',            settingController.createSetting);
router.delete('/:settingid',    settingController.deleteSetting);


module.exports = router;