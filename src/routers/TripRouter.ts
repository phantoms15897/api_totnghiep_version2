import * as express from "express";
import { TripController } from "../Controller/TripController";

const router = express.Router();

const tripController: TripController = new TripController();

router.get('/',             tripController.getTripList);
router.get('/searchtrip',      tripController.searchTrip);
router.get('/gettrip/:tripid',      tripController.getOneTrip);
router.get('/searchtripjoin/:userid',      tripController.searchTripJoin);
router.get('/searchtripcreate/:userid',      tripController.searchTripCreate);
router.patch('/:tripid',    tripController.updateTrip);
router.patch('/jointrip/:tripid',    tripController.joinTrip);
router.patch('/unjointrip/:tripid',    tripController.unjoinTrip);
router.post('/',            tripController.createTrip);
router.delete('/:tripid',   tripController.deleteTrip);


module.exports = router;