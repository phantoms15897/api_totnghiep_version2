import * as express from "express";
import { TypeNewsController } from "../Controller/TypeNewsController";

const router = express.Router();

const typeNewsController: TypeNewsController = new TypeNewsController();

router.get('/',          typeNewsController.getTypeNewsList);
router.get('/:typenewsid',   typeNewsController.searchTypeNews);
router.patch('/:typenewsid', typeNewsController.updateTypeNews);
router.post('/',   typeNewsController.createTypeNewsTrip);
router.delete('/:typenewsid',typeNewsController.deleteTypeNews);


module.exports = router;