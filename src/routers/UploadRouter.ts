import * as express from "express";
import { UploadController } from "../Controller/UploadController";

const router = express.Router();

const uploadController: UploadController = new UploadController();

const multer = require('multer');

const storage = multer.diskStorage({
    destination: function(req,file, cb){
        cb(null,'./uploads/images')
    },
    filename: function(req,file,cb){
        cb(null,Math.floor(Math.random()*99999999).toString() + file.originalname);
    }
});
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
    } else {
    cb(null, false);
    }
};
const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});
router.post('/multi',  upload.array('image',4), uploadController.uploadMulti);
router.post('/single', upload.single('image'), uploadController.uploadSingle);



module.exports = router;
