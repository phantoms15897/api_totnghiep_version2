import * as express from "express";
import { UserController } from "../Controller/UserController";
import { Auth } from "../midlleware/Auth";
const router = express.Router();
// const checkauth = require('../midlleware/Auth')
const userController: UserController = new UserController();
let auth = new Auth();
router.get('/',             userController.getAllUser);
router.get('/:userid',      userController.findUser);
router.patch('/:userid',    userController.updateUser);
router.post('/singup',      userController.signUp);
router.post('/login',       userController.logIn);
router.delete('/:userid',   userController.deleteUser);


module.exports = router;