import * as express from "express";
import { UserTripController } from "../Controller/UserTripController";

const router = express.Router();

const userTripController: UserTripController = new UserTripController();

router.get('/',             userTripController.getUserTripList);
router.get('/:usertripid',      userTripController.searchUserOnTrip);
router.post('/',            userTripController.createUserTrip);
router.patch('/:usertripid',    userTripController.updateUserTrip);
router.delete('/:usertripid',   userTripController.deleteUserTrip);


module.exports = router;